'program for GCA79

#define CONFIG = 0x3f50
//#define SIMULATION_WAITMS_VALUE = 0
All_Digital
CMCON = 7
TRISA = %00100111
PORTA = %00100111
TRISB = %11000000
PORTB = %11000000

Dim activation_time As Word
Dim dummy As Byte
Dim switch As Byte
//define actual positons (will be first calculated at start from inputs)
Dim ap_1 As Bit
Dim ap_2 As Bit
Dim ap_3 As Bit
Dim ap_4 As Bit
//define command inputs
Symbol ci1 = PORTA.0
Symbol ci2 = PORTA.1
Symbol ci3 = PORTA.2
Symbol ci4 = PORTA.5

//define feed-back
Symbol fb1 = PORTA.4
Symbol fb2 = PORTA.3
Symbol fb3 = PORTA.6
Symbol fb4 = PORTA.7

WaitMs 3000
Call get_positions()

Main:

//switch 1
ci1 = PORTA.0
If ci1 <> ap_1 Then
	ap_1 = ci1
	If ci1 = 1 Then  //*     switch straight
		Call set_position(1) //switch = 1
	Else  //*               switch round
		Call set_position(2) //switch = 2
	Endif
	fb1 = ap_1
Endif
//switch 2
ci2 = PORTA.1
If ci2 <> ap_2 Then
	ap_2 = ci2
	If ci2 = 1 Then  //*     switch straight
		Call set_position(3) //switch = 3
	Else  //*               switch round
		Call set_position(4) //switch = 4
	Endif
	fb2 = ap_2
Endif
//switch 3
ci3 = PORTA.2
If ci3 <> ap_3 Then
	ap_3 = ci3
	If ci3 = 1 Then  //*     switch straight
		Call set_position(5) //switch = 5
	Else  //*               switch round
		Call set_position(6) //switch = 6
	Endif
	fb3 = ap_3
Endif
//switch 4
ci4 = PORTA.5
If ci4 <> ap_4 Then
	ap_4 = ci4
	If ci4 = 1 Then  //*     switch straight
		Call set_position(7) //switch = 7
	Else  //*               switch round
		Call set_position(8) //switch = 8
	Endif
	fb4 = ap_4
Endif

Goto Main

End

Proc set_position(switch As Byte)
// first get dip switch settings
	dummy = 0
	dummy.0 = PORTB.7
	dummy.1 = PORTB.6
	Select Case dummy
	Case 3
		activation_time = 100
	Case 2
		activation_time = 250
	Case 1
		activation_time = 500
	Case Else
		activation_time = 2000
	EndSelect

	PORTB = LookUp(%11000011, %11000110, %11000101, %11001010, %11001001, %11010010, %11010001, %11100010, %11100001), switch
	WaitMs activation_time
	PORTB = %11000011
	switch = 0

End Proc

Proc get_positions()
	ap_1 = PORTA.0
	fb1 = ap_1
	ap_2 = PORTA.1
	fb2 = ap_2
	ap_3 = PORTA.2
	fb3 = ap_3
	ap_4 = PORTA.5
	fb4 = ap_4
End Proc


