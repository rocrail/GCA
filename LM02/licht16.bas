'Config = 0x31c4
Define CONF_WORD = 0x31c4

Dim i As Byte
Dim temp1 As Byte
Dim temp2 As Byte
Dim waarde(10) As Byte
Dim test As Byte
Dim n As Byte
Dim nieuw_adres As Byte
Dim adres_temp As Byte
Dim adres As Byte

CONFIG gpio.0 = Output
CONFIG gpio.1 = Output
CONFIG gpio.3 = Input
CONFIG gpio.2 = Output
CONFIG gpio.4 = Output
CONFIG gpio.5 = Output

cmcon = 7
ANSEL = 0

'als er geen adres is dan alle led tegelijk aan en weer uit
'als er een adres is dan alle leds 1 voor 1 aan en uit
adres = 2
'Goto begin

'Read 0, adres



If adres = 0xff Then
	gpio.0 = 1
	gpio.1 = 1
	gpio.2 = 1
	gpio.4 = 1
	gpio.5 = 1
	WaitMs 2000
	gpio.0 = 0
	gpio.1 = 0
	gpio.2 = 0
	gpio.4 = 0
	gpio.5 = 0
	WaitMs 2000
Else
	gpio.0 = 1
	WaitMs 500
	gpio.0 = 0
	gpio.1 = 1
	WaitMs 500
	gpio.1 = 0
	gpio.2 = 1
	WaitMs 500
	gpio.2 = 0
	gpio.4 = 1
	WaitMs 500
	gpio.4 = 0
	gpio.5 = 1
	WaitMs 500
	gpio.5 = 0
	WaitMs 500
Endif



begin:


'For n = 0 To 9
'waarde(n) = 0
'Next n

'' test = 0
'n = 0
'i = 0


Serin gpio.3, 9600, i  'test
If i = 72 Then
Serout gpio.1, 9600, "hello", #i, CrLf  'test
Endif
Goto begin


For n = 1 To 8
Serin gpio.3, 9600, test

waarde(n) = test
Next n

If waarde(3) = 80 And waarde(4) = 97 And waarde(5) = 97 And waarde(6) = 53 And waarde(7) = 53 And n = 9 Then Goto adresseren
If waarde(3) = 83 And n = 7 Then Goto set_output

Call laatzien(1)
Goto begin

adresseren:

If waarde(1) = 70 And waarde(2) = 70 Then  'F F
		'alle leds aanzetten
		'dan 2 volgende bytes inlezen
		'alle leds weer uit
		
	nieuw_adres = 16 * convert(waarde(8)) + convert(waarde(9))
	Write 0, nieuw_adres
	WaitMs 250
	Else
	nieuw_adres = 16 * convert(waarde(8)) + convert(waarde(9))
	Write 0, nieuw_adres
	WaitMs 250

Endif

Goto begin

set_output:
Call laatzien(2)
adres_temp = 16 * convert(waarde(1)) + convert(waarde(2))

'Read 0, adres
If adres_temp <> adres Then
Call laatzien(3)
Goto begin
Else
i = 16 * convert(waarde(4)) + convert(waarde(5))
Endif


temp1 = ShiftRight(i, 1)
temp2 = ShiftLeft(temp1, 1)
If temp2 = i Then gpio.0 = 0 Else gpio.0 = 1
i = ShiftRight(i, 1)


temp1 = ShiftRight(i, 1)
temp2 = ShiftLeft(temp1, 1)
If temp2 = i Then gpio.1 = 0 Else gpio.1 = 1
i = ShiftRight(i, 1)

temp1 = ShiftRight(i, 1)
temp2 = ShiftLeft(temp1, 1)
If temp2 = i Then gpio.2 = 0 Else gpio.2 = 1
i = ShiftRight(i, 1)

temp1 = ShiftRight(i, 1)
temp2 = ShiftLeft(temp1, 1)
If temp2 = i Then gpio.4 = 0 Else gpio.4 = 1
i = ShiftRight(i, 1)

temp1 = ShiftRight(i, 1)
temp2 = ShiftLeft(temp1, 1)
If temp2 = i Then gpio.5 = 0 Else gpio.5 = 1


Goto begin

End                                               

Function convert(arg1 As Byte) As Byte
If arg1 < 65 Then
convert = arg1 - 48
Endif


If arg1 > 65 Then
convert = arg1 - 55
Endif

End Function                                      

Proc laatzien(arg3 As Byte)
If arg3 = 1 Then
gpio.1 = 1
WaitMs 1000
gpio.5 = 0
Endif
If arg3 = 2 Then
gpio.2 = 1
WaitMs 1000
gpio.5 = 0
Endif

If arg3 = 3 Then
gpio.4 = 1
WaitMs 2000
gpio.5 = 0
WaitMs 2000

Endif

End Proc                                          