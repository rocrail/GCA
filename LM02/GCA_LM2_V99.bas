Define CONF_WORD = 0x33c4

'Define SIMULATION_WAITMS_VALUE = 1
'Program for Serial Programming Leds 8 bit version
'processor PIC16F688  8 MHz
AllDigital
TRISA = %001000
'TRISC = %000000
ConfigPin PORTC.0 = Output
ConfigPin PORTC.1 = Output
ConfigPin PORTC.2 = Output
ConfigPin PORTC.3 = Output
'CONFIG PORTC.4 = Output
ConfigPin PORTC.5 = Input

Dim character As Byte
Dim character_count As Byte  'this byte stores which received character is to be handled
Dim old_adress As Byte
Dim new_adress As Byte
Dim adress As Byte  'adress to be matched with decie_address
Dim device_adress As Byte
Dim leds As Byte
Dim command_type As Byte
Dim n As Byte


Symbol led1 = RC2
Symbol led2 = RC1
Symbol led3 = RC0
Symbol led4 = RA2
Symbol led5 = RA1
Symbol led6 = RA4
Symbol led7 = RA0
Symbol led8 = RA5

If RA3 = 0 Then
		Write 0, 255
	
WaitMs 1000
Endif


Hseropen 9600  'set serial receive to 9600 bd

Read 0, device_adress  'get the address of this chip


'********************************************

If device_adress = 255 Then
	Call set_leds(255)
	WaitMs 1000
	Call set_leds(0)
	WaitMs 1000
	Call set_leds(255)
	WaitMs 1000
	Call set_leds(0)
	WaitMs 1000

Else
	n = 128
	While n > 0
	Call set_leds(n)
	n = ShiftRight(n, 1)
	WaitMs 1000
	Wend
	WaitMs 1000
Endif
'********************************************

Call set_leds(0)  'leds off
command_type = 0
leds = 0
adress = 0

main:
	character_count = character_count + 1

	Hserin character
	If device_adress = 255 Then
		Hserout character
	Endif
		Select Case character_count
	Case 1
		If character <> 72 Then  '*  'H' received?
			character_count = 0  'wrong character , start again
		Endif
	Case 2
		Call convert_ascii()
		adress = character * 16
		old_adress = adress
	Case 3
		Call convert_ascii()
		adress = old_adress + character
		old_adress = adress
	Case 4
		If character = 83 Then  '*  'S'received
			command_type = 1
		Else
			If character = 80 Then  '*  'P' received
				command_type = 2
			Endif
		Endif
	Case 5
		Select Case command_type
		Case 1
			Call convert_ascii()
			leds = character * 16
		Case 2
			If character <> 97 Then  '*  'a'received?
				command_type = 0
				character_count = 0  'wrong character , start again
			Endif
		
		EndSelect
	Case 6
		Select Case command_type
		Case 1
			Call convert_ascii()
			leds = leds + character
		Case 2
			If character <> 97 Then  '*  'a'received?
				command_type = 0
				character_count = 0  'wrong character , start again
			Endif
				EndSelect
	Case 7
		Select Case command_type
		Case 1
			character_count = 0  'END OF TRANSMISSION
			If character = 13 Then  'only when CR received
				If adress = device_adress Then
					Call set_leds(leds)
				Endif
			Endif
		Case 2
			If character <> 53 Then  '*  '5'received?
				command_type = 0
				character_count = 0  'wrong character , start again
			Endif
				EndSelect
	Case 8
		Select Case command_type
		Case 2
			If character <> 53 Then  '*  '5'received?
				command_type = 0
				character_count = 0  'wrong character , start again
			Endif
				EndSelect
	Case 9
		Select Case command_type
		Case 2
			Call convert_ascii()
			new_adress = character * 16
		Case Else
		EndSelect
	Case 10
		Select Case command_type
		Case 2
			Call convert_ascii()
			new_adress = new_adress + character
		Case Else
		EndSelect
	Case 11
		If character = 13 Then
			If command_type = 2 Then
				Gosub setadress
				character_count = 0
			Endif
		Else
			character_count = 0
		Endif
	Case Else
	EndSelect
	
	
Goto main
End                                               

Proc set_leds(value As Byte)
	led1 = value.0
	led2 = value.1
	led3 = value.2
	led4 = value.3
	led5 = value.4
	led6 = value.5
	led7 = value.6
	led8 = value.7
End Proc                                          

setadress:
	If device_adress = old_adress Then
		Write 0, new_adress
				device_adress = new_adress
	Endif
Return                                            

Proc convert_ascii()  'convert ascii value to binairy
	If character > 57 Then
		character = character - 55  'A..F > 10..15'
	Else
		character = character - 48  '0..9
	Endif
End Proc                                          

