/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
 
 *******************************************************************************************
 ***** Using this hardware is at your own risk.                                        *****
 ***** Peter Giling and Robert Jan Versluis can not be held responsible for any damage *****
 ***** or problems by using the proposed hardware.                                     *****
 *******************************************************************************************
*/

The published hardware are prototypes, are incomplete and not ready for release.
Only original GCA Kits are supported.

https://wiki.rocrail.net/doku.php?id=gca:gca-index-en

Save your last working WIO firmaware before updating it.
