Define CONFIG1 = 0x0f84
Define CONFIG2 = 0x1cff

'program for current control and DCC feed-back
'started : apr 13 2020
'by Peter giling
TRISA = %00011111

ADCON1.ADFM = 1
ADCON1.ADPREF0 = 0
ADCON1.ADPREF1 = 0
ADCON1.ADCS2 = 0
ADCON1.ADCS1 = 0
ADCON1.ADCS0 = 0
ANSELA.ANSA4 = 0
ANSELA.ANSA2 = 0
ANSELA.ANSA1 = 1
ANSELA.ANSA0 = 1

'ADCON0.CHS0 = 1
'ADCON0.CHS1 = 0
'ADCON0.CHS2 = 0
'ADCON0.CHS3 = 0
ADCON0.CHS4 = 0
Symbol fb = RA5
Symbol cursel = RA4
Dim time1 As Byte
Dim time2 As Byte
Dim current_1 As Word
Dim current_2 As Word
Dim max_current_1 As Word
Dim max_current_2 As Word
Dim task As Byte
Dim dummy As Byte
Const max_timing = 25
Const max_current_prog = 55
Const puls_time = 60
max_current_1 = 100  '(r4 * 1 * 1024) / 5 ~~ 100   >>  R4 = 0,47 Ohm
If cursel = 0 Then
	max_current_1 = 336  '(r4 * 3,5 * 1024) / 5 = 336
Endif
max_current_2 = 963  '(r5 * 1 * 1024) / 5 = 963   >> R5 =4,7 Ohm
task = 0
dummy = 0
Main:
While dummy = 0
	task.0 = RA2
	task.1 = RA3
	Call getanalog()
	Select Case task
	Case 0  'sleep , do nothing
		fb = 0
		time1 = 0
		time2 = 0
	Case 2  'programming mode
		If current_2 > max_current_prog Then
			fb = 1
			WaitMs puls_time
			fb = 0
		Endif
	Case Else  'normal function, two outputs running.
		If current_1 > max_current_1 Then
			time1 = time1 + 1
			If time1 > max_timing Then
				time1 = max_timing
				fb = 1
			Endif
		Endif
		If current_2 > max_current_2 Then
			time2 = time2 + 1
			If time2 > max_timing Then
				time2 = max_timing
				fb = 1
			Endif
		Endif
	EndSelect
Wend

End                                               
Proc getanalog()
	Adcin 0, current_1
	Adcin 1, current_2

End Proc                                          
